<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="./img/favicon.ico" type="image/vnd.microsoft.icon" />

  <title>Validador de estrutura do Diploma Digital</title>

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
  <link rel="stylesheet" href="css/style.css">


  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-FZV907G9TH"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-FZV907G9TH');
</script>

</head>

<body>


  <header>
    <nav class="navbar navbar-expand-md navbar-dark bg-blue mb-5">
      <div class="container">
        <a class="navbar-brand" href="./index.php">
          <img src="./img/logo.jpg" alt="" class="img-fluid img-logo">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">

        <?php $activePage = basename($_SERVER['PHP_SELF'], ".php"); ?>

          <ul class="navbar-nav ms-auto mb-2 mb-md-0">
            <li class="nav-item">
              <a class="nav-link <?= ($activePage == 'diploma-xml' || $activePage == 'validar') ? 'active':''; ?>" aria-current="page" href="./diploma-xml.php">Validar XML Diploma</a>
            </li>

            
            <li class="nav-item">
              <a class="nav-link <?= ($activePage == 'documentacao-academica-xml' || $activePage == 'validar-xml-doc-academica') ? 'active':''; ?>" aria-current="page" href="./documentacao-academica-xml.php">Validar XML Doc. acadêmica</a>
            </li>



            <li class="nav-item">
              <a class="nav-link <?= ($activePage == 'documentacao-academica' || $activePage == 'validar-json-documentacao-academica') ? 'active':''; ?>" aria-current="page" href="./documentacao-academica.php">Validar JSON Doc. acadêmica</a>
            </li>

            
            <li class="nav-item">
              <a class="nav-link <?= ($activePage == 'diploma' || $activePage == 'validar-json-diploma') ? 'active':''; ?>" aria-current="page" href="./diploma.php">Validar JSON Diploma</a>
            </li>
            
            <li class="nav-item">
              <a class="nav-link <?= ($activePage == 'rvd' || $activePage == 'validar-json-rvd') ? 'active':''; ?>" aria-current="page" href="./rvd.php">
                Validar JSON Rep. Visual</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>