<?php

error_reporting(E_ERROR | E_PARSE);

$target_dir = "uploads/";
$filename =  basename(time() . "-" . $_FILES["diploma"]["name"]);
$target_file = $target_dir . $filename;

if (move_uploaded_file($_FILES["diploma"]["tmp_name"], $target_file)) {

  $dir = dirname(__FILE__);
  $file = $dir . '/uploads' . "/" . $filename;

  $xmlUpload = simplexml_load_file($file);

  $versaoXSD = $xmlUpload[0]->infDiploma['versao'];



  $xmlString = $xmlUpload->asXML();

  $word = '<Diploma xmlns="http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd">';




  switch ($versaoXSD) {
    case '1.02':
      $validadorXMLLocalCompleto = '<Diploma xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="DiplomaDigital_v1.02.xsd">';
      $validadorXMLLocal = 'xsi:noNamespaceSchemaLocation="DiplomaDigital_v1.02.xsd"';
      break;

    case '1.03':
      $validadorXMLLocalCompleto = '<Diploma xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="DiplomaDigital_v1.03.xsd">';
      $validadorXMLLocal = 'xsi:noNamespaceSchemaLocation="DiplomaDigital_v1.03.xsd"';
      break;

    case '1.04.1':
      $validadorXMLLocalCompleto = '<Diploma xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="DiplomaDigital_v1.04.1.xsd">';
      $validadorXMLLocal = 'xsi:noNamespaceSchemaLocation="DiplomaDigital_v1.04.1.xsd"';
      break;

    default:
      $validadorXMLLocalCompleto = '<Diploma xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="DiplomaDigital_v1.03.xsd">';
      $validadorXMLLocal = 'xsi:noNamespaceSchemaLocation="DiplomaDigital_v1.03.xsd"';
      break;
  }


  if (strpos($xmlString, $word) !== false) {
    $xmlPadronizado =  str_replace(
      '<Diploma xmlns="http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd">',
      $validadorXMLLocalCompleto,
      $xmlString
    );
  } else {
    $xmlPadronizado =  str_replace(
      'xmlns="http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd"',
      $validadorXMLLocal,
      $xmlString
    );
  }

  $doc = new DOMDocument;
  $doc->documentURI = $file;
  $doc->loadXML($xmlPadronizado);
  $doc->xinclude();

  $errs = [];
  set_error_handler(function ($severity, $message, $file, $line) use (&$errs) {
    $errs[] = $message;
  });


  switch ($versaoXSD) {
    case '1.02':
      $IsValido = $doc->schemaValidate($dir . '/uploads' . "/" . 'DiplomaDigital_v1.02.xsd') ? 'Válida' : 'Inválida';
      break;

    case '1.03':
      $IsValido = $doc->schemaValidate($dir . '/uploads' . "/" . 'DiplomaDigital_v1.03.xsd') ? 'Válida' : 'Inválida';
      break;

    case '1.04.1':
      $IsValido = $doc->schemaValidate($dir . '/uploads' . "/" . 'DiplomaDigital_v1.04.1.xsd') ? 'Válida' : 'Inválida';
      break;

    default:
      $IsValido = $doc->schemaValidate($dir . '/uploads' . "/" . 'DiplomaDigital_v1.03.xsd') ? 'Válida' : 'Inválida';
      break;
  }
} else {

  switch ($_FILES['diploma']['error']) {
    case UPLOAD_ERR_OK:
      break;
    case UPLOAD_ERR_NO_FILE:
      throw new RuntimeException('Arquivo não enviado.');
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
      throw new RuntimeException('Limite excedido.');
    default:
      throw new RuntimeException('Erro desconnhecido.');
  }
  echo "Desculpe, ocorreu um erro ao enviar seu arquivo.";
}

?>

<?php require_once("header.php"); ?>


<main>

  <div class="container mt-5 report">

    <div class="row">
      <h1 class="text-center">A estrutura do arquivo <em>"<?= $_FILES["diploma"]["name"] ?>"</em> é <strong>
          <?= $IsValido =  $IsValido == "Válida" ? "<span class='text-success'>$IsValido</span>" : "<span class='text-danger'>$IsValido</span>"; ?></strong> </h1>

      <h4 class="text-center">Versão do XSD <strong><?= $versaoXSD ?></strong></h4>
    </div>

    <div class="row mt-3">

      <?php
      foreach ($errs as &$e) {
        echo "<div class='alert alert-danger' role='alert'>$e</div>";
      }
      restore_error_handler();
      ?>

    </div>


    <div class="row d-flex justify-content-center mt-5 mb-8">
      <a href="#s" id="imprimir" class="btn btn-info btn-voltar mx-3 ">Imprimir</a>
      <a href="./diploma-xml.php" class="btn btn-warning btn-voltar">Voltar</a>
    </div>
  </div>
</main>

<script src="./js/print.js"></script>

<?php require_once("footer.php"); ?>