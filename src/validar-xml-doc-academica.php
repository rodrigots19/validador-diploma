<?php

error_reporting(E_ERROR | E_PARSE);

//Constantes 
const VALIDA = "Válida";
const INVALIDA = "Inválida";
const TAG_DOC_ACA_REG = '<DocumentacaoAcademicaRegistro xmlns="http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd">';


$target_dir = "uploads/";
$filename =  basename(time() . "-" . $_FILES["doc-acad"]["name"]);
$target_file = $target_dir . $filename;

if (move_uploaded_file($_FILES["doc-acad"]["tmp_name"], $target_file)) {

  $dir = dirname(__FILE__);
  $file = $dir . '/' . $target_dir . $filename;

  $xmlUpload = simplexml_load_file($file);

  $versaoXSD = $xmlUpload[0]->RegistroReq['versao'] <> null ? $xmlUpload[0]->RegistroReq['versao'] : $xmlUpload[0]->RegistroSegundaViaReq['versao'];

  $xmlString = standardizeXml($xmlUpload->asXML());

  $IsDiploma = checkDiploma($xmlString );

  switch ($versaoXSD) {
    case '1.02':
      $validadorXMLLocalCompleto = '<DocumentacaoAcademicaRegistro xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="DocumentacaoAcademicaRegistroDiplomaDigital_v1.02.xsd">';
      $validadorXMLLocal = 'xsi:noNamespaceSchemaLocation="DocumentacaoAcademicaRegistroDiplomaDigital_v1.02.xsd';
      break;

    case '1.03':
      $validadorXMLLocalCompleto = '<DocumentacaoAcademicaRegistro xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="DocumentacaoAcademicaRegistroDiplomaDigital_v1.03.xsd">';
      $validadorXMLLocal = 'xsi:noNamespaceSchemaLocation="DocumentacaoAcademicaRegistroDiplomaDigital_v1.03.xsd';
      break;

    case '1.04.1':
      $validadorXMLLocalCompleto = '<DocumentacaoAcademicaRegistro xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="DocumentacaoAcademicaRegistroDiplomaDigital_v1.04.1.xsd">';
      $validadorXMLLocal = 'xsi:noNamespaceSchemaLocation="DocumentacaoAcademicaRegistroDiplomaDigital_v1.04.1.xsd';
      break;

    default:
      $validadorXMLLocalCompleto = '<DocumentacaoAcademicaRegistro xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="DocumentacaoAcademicaRegistroDiplomaDigital_v1.03.xsd">';
      $validadorXMLLocal = 'xsi:noNamespaceSchemaLocation="DocumentacaoAcademicaRegistroDiplomaDigital_v1.03.xsd';
      break;
  }

  if (strpos($xmlString, TAG_DOC_ACA_REG) !== false) {
    $xmlPadronizado =  str_replace(
      TAG_DOC_ACA_REG,
      $validadorXMLLocalCompleto,
      $xmlString
    );
  } else {
    $xmlPadronizado =  str_replace(
      'xmlns="http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd"',
      $validadorXMLLocal,
      $xmlString
    );
  }

  $doc = new DOMDocument;
  $doc->documentURI = $file;
  $doc->loadXML($xmlPadronizado);
  $doc->xinclude();

  $errs = [];
  set_error_handler(function ($severity, $message, $file, $line) use (&$errs) {
    $errs[] = $message;
  });

  switch ($versaoXSD) {
    case '1.02':
      $IsValido = $doc->schemaValidate($dir . '/' . $target_dir . 'DocumentacaoAcademicaRegistroDiplomaDigital_v1.02.xsd') ? VALIDA : INVALIDA;
      break;

    case '1.03':
      $IsValido = $doc->schemaValidate($dir . '/' . $target_dir . 'DocumentacaoAcademicaRegistroDiplomaDigital_v1.03.xsd') ? VALIDA : INVALIDA;
      break;

    case '1.04.1':
      $IsValido = $doc->schemaValidate($dir . '/' . $target_dir . 'DocumentacaoAcademicaRegistroDiplomaDigital_v1.04.1.xsd') ? VALIDA : INVALIDA;
      break;

    default:
      $IsValido = $doc->schemaValidate($dir . '/' . $target_dir . 'DocumentacaoAcademicaRegistroDiplomaDigital_v1.03.xsd') ? VALIDA : INVALIDA;
      break;
  }
} else {
  switch ($_FILES['diploma']['error']) {
    case UPLOAD_ERR_OK:
      break;
    case UPLOAD_ERR_NO_FILE:
      throw new UnexpectedValueException('Arquivo não enviado.');
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
      throw new UnexpectedValueException('Limite excedido.');
    default:
      throw new UnexpectedValueException('Erro desconnhecido.');
  }
  echo "Desculpe, ocorreu um erro ao enviar seu arquivo.";
}

//Sanitiza o arquivo XML 
function standardizeXml(string $xml_string){
  $xml_string = preg_replace('/ xmlns:xsi[^=]*="[^"]*"/i', '', $xml_string);
  $xml_string = preg_replace('/ xmlns:xsd[^=]*="[^"]*"/i', '', $xml_string);
  $xml_string = preg_replace('/ xsi:noNamespaceSchemaLocation[^=]*="[^"]*"/i', '', $xml_string);
  $xml_string = preg_replace('/ noNamespaceSchemaLocation[^=]*="[^"]*"/i', '', $xml_string);
  $xml_string = preg_replace('/ xsi:schemaLocation[^=]*="[^"]*"/i', '', $xml_string);

  $find = array('<DocumentacaoAcademicaRegistro>', '<DocumentacaoAcademicaRegistro  >');
  $replace = array('<DocumentacaoAcademicaRegistro xmlns="http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd">');
  return $xml_string = str_replace($find, $replace, $xml_string);
}


function checkDiploma(string $xmlString){
  return  strpos($xmlString, "<infDiploma") == true ? true : false;
}

?>

<?php require_once("header.php"); ?>


<main>

  <div class="container mt-5">

    <div class="row">
      <h1 class="text-center">A estrutura do arquivo <em>"<?= $_FILES["doc-acad"]["name"] ?>"</em> é <strong>
          <?= $IsValido =  $IsValido == "Válida" ? "<span class='text-success'>$IsValido</span>" : "<span class='text-danger'>$IsValido</span>"; ?></strong> </h1>
      <h4 class="text-center">Versão do XSD <strong><?= $versaoXSD ?></strong></h4>

    </div>

    <div class="row mt-3">

      <?php
      foreach ($errs as &$e) {   
        if ($IsDiploma) {
          echo "<div class='alert alert-warning text-center' role='alert'>
          O arquivo enviado não é uma documentação acadêmica! Por favor, verfique o arquivo.
          <a href='./diploma-xml.php'>Caso deseje validar um diploma digital, clique aqui para verificar!</a>          
          </div>";
        } else {
          echo "<div class='alert alert-danger' role='alert'>$e</div>";
        }
      }
      restore_error_handler();
      ?>

    </div>


    <div class="row d-flex justify-content-center mt-5 mb-8">
      <a href="#s" id="imprimir" class="btn btn-info btn-voltar mx-3 ">Imprimir</a>
      <a href="./documentacao-academica-xml.php" class="btn btn-warning btn-voltar">Voltar</a>
    </div>
  </div>
</main>

<script src="./js/print.js"></script>

<?php require_once("footer.php"); ?>