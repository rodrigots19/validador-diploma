<?php

require 'vendor/autoload.php';

$target_dir = "uploads/";
$filename =  basename(time() . "-" . $_FILES["json-rvd"]["name"]);
$target_file = $target_dir . $filename;

if (move_uploaded_file($_FILES["json-rvd"]["tmp_name"], $target_file)) {
  $dir = dirname(__FILE__);
  $file = $dir . '/uploads' . "/" . $filename;

  $data = json_decode(file_get_contents($file));

  $validator = new JsonSchema\Validator;
  $validator->validate($data, (object)['$ref' => 'file://' . realpath($dir . DIRECTORY_SEPARATOR . 'schemas'.  DIRECTORY_SEPARATOR . 'RepVisualJsonSchema_v1.03.json')]);

}else{

  switch ($_FILES['json-doc-aca']['error']) {
    case UPLOAD_ERR_OK:
      break;
    case UPLOAD_ERR_NO_FILE:
      throw new RuntimeException('Arquivo não enviado.');
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
      throw new RuntimeException('Limite excedido.');
    default:
      throw new RuntimeException('Erro desconnhecido.');
  }

  var_dump($_FILES['json-doc-aca']['error']);
  echo "Desculpe, ocorreu um erro ao enviar seu arquivo.";

}
?>


<?php require_once("header.php"); ?>

<main>

  <div class="container mt-5">

    <div class="row">
      <h1 class="text-center">A estrutura do arquivo <em>"<?= $_FILES["json-rvd"]["name"] ?>"</em> é <strong>
          <?= $validator->isValid() === true ? "<span class='text-success'>Válida</span>" : "<span class='text-danger'>Inválida</span>"; ?>
        </strong> </h1>
    </div>

    <div class="row mt-3">

      <?php
      if ($validator->isValid()) {
        echo "";
      } else {
        foreach ($validator->getErrors() as $error) {
          $property = $error["property"];
          $message = $error["message"];

          echo "<div class='alert alert-danger' role='alert'>$property - $message</div>";
          echo "<br>";
        }
      }
      ?>

    </div>


    <div class="row d-flex justify-content-center mt-5 mb-8">
      <a href="#s" id="imprimir" class="btn btn-info btn-voltar mx-3 ">Imprimir</a>
      <a href="./rvd.php" class="btn btn-warning btn-voltar">Voltar</a>
    </div>
  </div>
</main>

<script src="./js/print.js"></script>

<?php require_once("footer.php"); ?>