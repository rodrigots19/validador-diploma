<?php require_once("header.php"); ?>
<main>

  <div class="container cards-home">

    <div class="row mt-5 d-flex justify-content-center">

      <div class="col-md-4">

        <div class="card">
          <h5 class="card-header">TREM BOM</h5>
          <div class="card-body">
            <h5 class="card-title">Verificar a conformidade de estrutura do Diploma</h5>
            <p class="card-text">Acesse a página e faça upload do arquivo .XML e depois clique em verificar.</p>
            <a href="./diploma-xml.php" class="btn btn-primary"> <i class="bi bi-search"></i> Validar</a>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card ">
          <h5 class="card-header">Validar XML da Doc. acadêmica</h5>
          <div class="card-body">
            <h5 class="card-title">Verificar a conformidade de estrutura da Doc. acadêmica</h5>
            <p class="card-text">Acesse a página e faça upload do arquivo .XML e depois clique em verificar.</p>
            <a href="./documentacao-academica-xml.php" class="btn btn-primary"> <i class="bi bi-search"></i> Validar</a>
          </div>
        </div>
      </div>


    </div>

    <div class="row mt-5 d-flex justify-content-center">

      <div class="col-md-4">
        <div class="card">
          <h5 class="card-header">Validar JSON Doc. acadêmica</h5>
          <div class="card-body">
            <h5 class="card-title">Verificar a conformidade de estrutura do JSON</h5>
            <p class="card-text">Acesse a página e faça upload do arquivo .JSON e depois clique em verificar.</p>
            <a href="./documentacao-academica.php" class="btn btn-primary"> <i class="bi bi-search"></i> Validar</a>
          </div>
        </div>
      </div>

      <div class="col-md-4">

        <div class="card">
          <h5 class="card-header">Validar JSON Diploma</h5>
          <div class="card-body">
            <h5 class="card-title">Verificar a conformidade de estrutura do JSON</h5>
            <p class="card-text">Acesse a página e faça upload do arquivo .JSON e depois clique em verificar.</p>
            <a href="./diploma.php" class="btn btn-primary"> <i class="bi bi-search"></i> Validar</a>
          </div>
        </div>
      </div>

      <div class="col-md-4">

        <div class="card">
          <h5 class="card-header">Validar JSON Rep. Visual</h5>
          <div class="card-body">
            <h5 class="card-title">Verificar a conformidade de estrutura do JSON</h5>
            <p class="card-text">Acesse a página e faça upload do arquivo .JSON e depois clique em verificar.</p>
            <a href="./rvd.php" class="btn btn-primary"> <i class="bi bi-search"></i> Validar</a>
          </div>
        </div>
      </div>
    </div>

  </div>

</main>
<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>

<?php require_once("footer.php"); ?>
