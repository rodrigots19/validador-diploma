<?php require_once("header.php"); ?>
<main class="main-validator">

  <div class="container mt-5 d-flex justify-content-center ">

    <div class="card col-md-6 mt-5">
      <div class="card-body">
        <h2 class="card-title text-center">Verificar JSON Documentação Acadêmica</h2>
  

        <form action="validar-json-documentacao-academica.php" method="post" enctype="multipart/form-data" class="form-flex">
          <label for="">Faça upload do arquivo .json e depois clique em verificar.</label>

          <i class="bi bi-filetype-json"></i>

          <input type="file" name="json-doc-aca" id="json-doc-aca" accept="application/JSON" class="btn btn-primary btn-upload mt-3" required hidden>

          <label for="json-doc-aca" class="label-btn-diploma"><i class="bi bi-cloud-upload-fill"></i> Escolher arquivo</label>

          <span id="selected_filename"></span>

          <input id="verificar" type="submit" value="Verificar" name="Verificar" class="btn btn-success btn-validar mt-1" disabled>
        </form>

      </div>
    </div>

  </div>

</main>
<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
<script>
  $('#json-doc-aca').change(function() {
    $('#selected_filename').text($('#json-doc-aca')[0].files[0].name);
    $("#verificar").prop("disabled", false);

  });
</script>

<?php require_once("footer.php"); ?>