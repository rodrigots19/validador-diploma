FROM php:7.4-apache

WORKDIR /var/www/public

COPY ./config/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY ./config/start-apache /usr/local/bin
COPY ./src /var/www/public
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
#COPY ./config/php.ini /usr/local/etc/php/php.ini
COPY ./config/docker-php-ext-uploads.ini /usr/local/etc/php/conf.d/docker-php-ext-uploads.ini


RUN apt-get update && apt-get install git -y

RUN chmod +x /usr/local/bin/start-apache &&\
    chown -R www-data:www-data /var/www/public &&\
    composer install

CMD ["start-apache"]
